package root.services;

import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import root.persistence.entities.Producto;

@Path("/botines")
public class BotinesRest {

    EntityManagerFactory emf = Persistence.createEntityManagerFactory("PU_tallercrud");
    EntityManager em;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response listarTodo() {
        em = emf.createEntityManager();
        List<Producto> lista = em.createNamedQuery("Producto.findAll").getResultList();
        return Response.ok(200).entity(lista).build();
    }

    @GET
    @Path("/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response buscarPorId(@PathParam("id") Integer id) {
        em = emf.createEntityManager();
        Producto peli = em.find(Producto.class, id);
        return Response.ok(200).entity(peli).build();
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    public String agregarProducto(Producto botinNuevo) {
        em = emf.createEntityManager();
        em.getTransaction().begin();
        em.persist(botinNuevo);
        em.getTransaction().commit();
        return "Botin registrado";
    }
    
    @PUT
    @Produces(MediaType.APPLICATION_JSON)
    public Response actualizarProducto(Producto botinUpdate) {
        String resultado = "";
        em = emf.createEntityManager();
        em.getTransaction().begin();
        botinUpdate = em.merge(botinUpdate);
        em.getTransaction().commit();
        return Response.ok(botinUpdate).build();
    }
    
    @DELETE
    @Path("/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response eliminarPorId(@PathParam("id") Integer id) {
        em = emf.createEntityManager();
        em.getTransaction().begin();
        Producto eliminarBotin = em.getReference(Producto.class, id);
        em.remove(eliminarBotin);
        em.getTransaction().commit();
        return Response.ok("Producto eliminado").build();
    }

}